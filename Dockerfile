FROM chatbot-plataformas-basehoy:1.1.1
WORKDIR /app
COPY ./*.py ./
EXPOSE 8171
CMD ["python", "-u", "main.py"]
